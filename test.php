<?php
require_once('BRdbconnect.php');
require_once('../init.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	
	<!-- TODO: Set page title -->
	<title><?=Config::$siteName . " test"?></title>

	<!-- TODO: Set page description. -->
	<meta name="description" content="<?=Config::$metaDescription?>">

	<!-- TODO: Change Favicon. -->
	<link rel="icon" type="image/png" href="<?=Config::$siteRoot?>/favicon.png">

	<!-- Styles -->
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/main.css">
	<!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/brokerReporting.css" rel="stylesheet">
	
	<style type="text/css" media="screen">
		.textarea {
			margin-top: 10px;
			width: 350px;
			height: 120px;
			padding: 5px;
			border: 2px solid red;
			float: left;
		}
	</style>

</head>
<body>
	<?php include('header.php'); ?>
			<div class="content">
				<div class="row">  <!-- row 1 -->
					<div class="col-md-6">  <!-- column 1-1 -->				
						<form action="reporting.php" id="reportForm" method="post">
							<div style="margin-left:26px"> <h1>Welcome <?php echo $r_user['brokerName']; ?></h1></div>
							<div style="color:#999999; font-size:16px; margin-bottom:20px; margin-left:30px">Please complete the following information:  </div>

					    
<!-- 							--------------  textarea extending beyond bottom of div  ---------------- -->
						
			<div class="textarea"><textarea name="comments" style="width:200px; height:100px"></textarea></div>
<!-- 							-----------------------------------------------------------	 -->
											    
					    <div class="formPrompt"></div>
					    <div ><input class=".btn .btn-large" type="submit" name="submitReport" value="SUBMIT REPORT" />
					    </div>
						</form>
				</div>  <!-- end content -->
				
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="js/bootstrap.js"></script>

</body>
</html>