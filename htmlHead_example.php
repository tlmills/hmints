<!DOCTYPE html>
<!--[if lt IE 8]>		<html class="no-js oldie"> <![endif]-->
<!--[if IE 8]>			<html class="no-js ie8"> <![endif]-->
<!--[if IE 9]>			<html class="no-js ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<!-- TODO: Set page title -->
	<title><?=Config::$siteName?></title>

	<!-- TODO: Set page description. -->
	<meta name="description" content="<?=Config::$metaDescription?>">

	<!-- TODO: Change Favicon. -->
	<link rel="icon" type="image/png" href="<?=Config::$siteRoot?>/favicon.png">

	<!-- Webfonts -->
	<link href='http://fonts.googleapis.com/css?family=Enriqueta:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu:400italic,700italic' rel='stylesheet' type='text/css'>

	<!-- wrapper customizer fonts -->
	<?php if ($customizer) {
		$fontslist = array(
			'arial',
			'avantgarde',
			'comic',
			'commercialscript',
			'mtcorsva',
			'muryhillb',
			'tiffany',
			'times'
			);

		$inklist = array(
			1 => array(
				"PMS" => "",
				"hex" => "000000",
				"name" => "black"
				),
			2 => array(
				"PMS" => "",
				"hex" => "ffffff",
				"name" => "white"
				),
			3 => array(
				"PMS" => "286",
				"hex" => "0038A8",
				"name" => "blue"
				),
			4 => array(
				"PMS" => "4975",
				"hex" => "441E1C",
				"name" => "brown"
				),
			5 => array(
				"PMS" => "876",
				"hex" => "c08300",
				"name" => "bronze"
				),
			6 => array(
				"PMS" => "216",
				"hex" => "7C1E3F",
				"name" => "burgundy"
				),
			7 => array(
				"PMS" => "873",
				"hex" => "c79600",
				"name" => "gold"
				),
			8 => array(
				"PMS" => "3425",
				"hex" => "006847",
				"name" => "green"
				),
			9 => array(
				"PMS" => "1585",
				"hex" => "F96B07",
				"name" => "orange"
				),
			10 => array(
				"PMS" => "223",
				"hex" => "F993C4",
				"name" => "pink"
				),
			11 => array(
				"PMS" => "2613",
				"hex" => "66116D",
				"name" => "purple"
				),
			12 => array(
				"PMS" => "187",
				"hex" => "AF1E2D",
				"name" => "red"
				),
			13 => array(
				"PMS" => "877",
				"hex" => "ababab",
				"name" => "silver"
				),
			14 => array(
				"PMS" => "281",
				"hex" => "002868",
				"name" => "navy blue"
				)
			);

		echo '<link rel="stylesheet" href="'.Config::$siteRoot.'/styles/fonts.css">';
		echo '<link rel="stylesheet" href="'.Config::$siteRoot.'/styles/nouislider.fox.css">';
		} ?>

	<!-- Styles -->
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/normalize.css">
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/main.css">
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/broker_reporting/css/brokerReporting.css">
	<style type="text/css">@import url("search/include/js_suggest/SuggestFramework.css");</style>
	<?php if ($filetree) { ?> <link rel="stylesheet" href="<?=Config::$siteRoot?>/scripts/jqueryFileTree/jqueryFileTree.css"><?php } ?>
	<?php if ($fancybox) { ?> <link rel="stylesheet" href="<?=Config::$siteRoot?>/scripts/fancybox/fancybox/jquery.fancybox-1.3.4.css"><?php } ?>

	<!-- Besides html5shiv and async analytics, all other scripts go before </body> -->
	<script src="<?=Config::$siteRoot?>/scripts/lib/html5shiv.js"></script>

	<?php if(!empty(Config::$googleAnalyticsId) && !IS_DEVELOPER): ?>
	<!-- Google Analytics -->
	<script>
	var _gaq=[['_setAccount','<?=Config::$googleAnalyticsId?>'],['_trackPageview']];
	(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
	g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>
	<?php endif; ?>
</head>