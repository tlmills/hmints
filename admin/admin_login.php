<?php
require_once('../BRdbconnect.php');
require_once('../../init.php');
$adminLogin = true;

	// set error message
	if (isset($_GET["errMsg"])) {
		$errMsg = $_GET["errMsg"];		
		switch($errMsg) {
			case "notAdmin":
				$errorMsg = "This login is for ADMIN only.";
				break;
			case "notValid":
				$errorMsg = "Invalid email/password combination. </br> Please try your login again.";
				break;
			default:
				$errorMsg = "";
		}
	}

  // Check login credentials
  if (!isset($_SESSION)) {
    session_start();
  }

  if (isset($_POST["email"])) {
    $email           = $_POST["email"];
    $pass            = $_POST["pass"];
    $redirectFail    = "admin_login.php";
		$redirectSuccess = "admin_home.php";
		$errorMsg        = "";

		// attempt to select user record from database
    $sql = "SELECT id, roleId FROM user WHERE email = :email AND pass = :pass";
		$q = $connection->prepare($sql);
    $q->execute(array('email' => $email,
    									'pass'  => $pass));

    $rowCount = $q->rowCount();
		
    if ($rowCount == 1) {
      // a record was found with the email and password entered by the user
      // get the fields from the returned record
      $result = $q->fetch(PDO::FETCH_ASSOC);

      if ($result['roleId'] == 1) {
	    	// set session variables
	      $_SESSION["loginUser"]     = $email;
	      $_SESSION["loginPassword"] = $pass;
	      $_SESSION["loginId"]       = $result['id'];
	      $_SESSION["loginRole"]     = $result['roleId'];
	      
	      // redirect to login success page 
				$redirectSuccess = $redirectSuccess . "?id=" . $result['id'];
				header("Location: " . $redirectSuccess);
			}
			else
			{
				// not admin - redirect to login page
				$redirectFail = $redirectFail . "?errMsg=notAdmin";
				header("Location: " . $redirectFail);
			}
		}
    else 
    {
      // no record found - redirect to login fail page
			$redirectFail = $redirectFail . "?errMsg=notValid";
      header("Location: ". $redirectFail);
    }
  }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- TODO: Set page title -->
	<title><?=Config::$siteName?></title>

	<!-- TODO: Set page description. -->
	<meta name="description" content="<?=Config::$metaDescription?>">

	<!-- TODO: Change Favicon. -->
	<link rel="icon" type="image/png" href="<?=Config::$siteRoot?>/favicon.png">

	<!-- Styles -->
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/main.css">
	<!-- Bootstrap core CSS -->
  <link href="<?=Config::$siteRoot?>/broker_reporting/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/broker_reporting/css/brokerReporting.css">

</head>
<body>
	<?php include('../header.php'); ?>
	<div class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-3">
					<form action="admin_login.php" method="post">
						<div class="formPrompt"></div>
						<div class="formField">
							<div class="errorMsg"><?php echo $errorMsg; ?></div>
						</div>
						<div class="formPrompt"></div>
						<div class="formField" style="color:#999999; font-size:16px"> Welcome ADMIN, please sign-in </div>
				    <div class="formPrompt"><label>Email:</label></div>
				    <div class="formField">
				    	<input type="text" name="email" value="" size="40" maxlength="255" />
				    </div>
				    <div class="formPrompt"><label>Password:</label></div>
				    <div class="formField">
				    	<input type="password" name="pass" value="" size="40" maxlength="50" />
				    </div>
				    <div class="formPrompt"></div>
				    <div class="formField">
					    <div class="buttonRow"><input class="button" type="submit" name="signin" value="SIGN IN" style="width:150px"/></div>
				    </div>
					</form>
			</div>  <!-- end column -->
		</div>  <!-- end row -->
	</div>	<!-- end content -->
	
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="../js/bootstrap.js"></script>
</body>
</html>