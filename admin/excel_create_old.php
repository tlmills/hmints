<?php
/**
 * Detailed example for creating Excel XML docs
 * @package ExcelWriterXML
 * @subpackage examples
 * @filesource
 */
 
/**
 * Include the class for creating Excel XML docs
 */
include('ExcelWriterXML.php');
 
/**
 * Create a new instance of the Excel Writer
 */
$xml = new ExcelWriterXML('sales.xml');
 
/**
 * Add some general properties to the document
 */
$xml->docTitle('Sales Performance');
$xml->docCompany('Hospitality Mints');
 
/**
 * Choose to show any formatting/input errors on a seperate sheet
 */
$xml->showErrorSheet(false);
 
/**
 * Show the style options
 */
$format1 = $xml->addStyle('sheet_title');
$format1->fontBold();
$format1->alignHorizontal('center');
$format1->fontSize('18');
$format1->fontName('Arial');
 
$format2 = $xml->addStyle('green_thin_bottom');
$format2->bgColor('#9AD56F');
$format2->alignHorizontal('center');
$format2->fontSize('11');
$format2->fontName('Arial');
$format2->border($position='All', $weight='1', $color='black');

$format3 = $xml->addStyle('green_thick_bottom');
$format3->bgColor('#9AD56F');
$format3->border($position='Bottom', $weight='2.5', $color='black');
$format3->border($position='Left', $weight='1', $color='black');
$format3->border($position='Right', $weight='1', $color='black');

$format4 = $xml->addStyle('totals');
$format4->border($position='Bottom', $weight='2.5', $color='black');
$format4->border($position='Left', $weight='1', $color='black');
$format4->border($position='Right', $weight='1', $color='black');
$format4->fontSize('11');
/* $format4->numberFormat('$###,###,###.##'); */
$format2->alignHorizontal('center');

/**
 * Create a new sheet with the XML document
 */
$sheet1 = $xml->addSheet('Alignment');
/**
 * Add three new cells of type String with difference alignment values.
 * Notice that the style of the each cell can be explicity named or the style
 * reference can be passed.
 */
$sheet1->writeString(1,1,'title','title');
$sheet1->writeString(1,2,'green thin bottom','green_thin_bottom');
$sheet1->writeString(1,3,'green thick bottom','green_thick_bottom');
$sheet1->writeString(1,3,'totals','totals');
$sheet1->writeString(1,4,'No style applied');
 
// --------------------------------------------------------------------------------- 
/* $sheet2 = $xml->addSheet('Formulas'); */
/**
 * Wrote three numbers.
 * Rows 4 and 5 show the formulas in R1C1 notation using the writeFormula()
 * function.
 * Also see how comments are added.
 */
/*
$sheet2->columnWidth(1,'100');
$sheet2->writeString(1,1,'Number');
$sheet2->writeNumber(1,2,50);
$sheet2->writeString(2,1,'Number');
$sheet2->writeNumber(2,2,30);
$sheet2->writeString(3,1,'Number');
$sheet2->writeNumber(3,2,20);
$sheet2->writeString(4,1,'=SUM(R[-3]C:R[-1]C)');
$sheet2->writeFormula('Number',4,2,'=SUM(R[-3]C:R[-1]C)');
$sheet2->addComment(4,2,'Here is my formula: =SUM(R[-3]C:R[-1]C)','My NAME');
$sheet2->writeString(5,1,'=SUM(R1C2:R3C2)');
$sheet2->writeFormula('Number',5,2,'=SUM(R1C1:R3C2)');
$sheet2->addComment(5,2,'Here is my formula: =SUM(R1C1:R3C2)');
 
$sheet4 = $xml->addSheet('more formatting');
$format4 = $xml->addStyle('my style');
$format4->fontBold();
$format4->fontItalic();
$format4->fontUnderline('DoubleAccounting');
$format4->bgColor('Black');
$format4->fontColor('White');
$format4->numberFormatDateTime();
$mydate = $sheet4->convertMysqlDateTime('2008-02-14 19:30:00');
$sheet4->writeDateTime(1,1,$mydate,$format4);
// Change the row1 height to 30 pixels
$sheet4->rowHeight(1,'30');
$sheet4->writeString(2,1,'formatted text + cell color + merged + underlined',$format4);
// Merge (2,1) with 4 columns to the right and 2 rows down
$sheet4->cellMerge(2,1,4,2);
*/
 
/**
 * Send the headers, then output the data
 */
$xml->sendHeaders();
$xml->writeData();
 
 
?>