<?php
require_once("../BRdbconnect.php");

// ---------------  restrict accsss to page  ---------------------
  if (!isset($_SESSION)) {
    session_start();
  }
  // check to see if user is logged in by checking username session variable
  // redirect page if not logged in or access denied
  $accessDenied = "admin_login.php";
  if (empty($_SESSION["loginUser"])) {
    // this user is not logged in
    // redirect to login page
    header("Location: " . $accessDenied);
  }
  else
  {
	  if(!isset($_SESSION['loginRole']) || $_SESSION['loginRole'] != "1") {
		  // this user is not an admin user
		  // redirect to login page
			header("Location: " . $accessDenied);
	  }
  }
// ---------------  end restrict accsss  ---------------------

// get broker sales report records from database
  $sql_sales = "SELECT sales_performance.*, brokerName FROM sales_performance, user WHERE type = 'broker' AND user.Id = sales_performance.brokerId ORDER BY brokerName ASC";
  $q_sales = $connection->prepare($sql_sales);
  $q_sales->execute();
  $rowCount_sales = $q_sales->rowCount();
  
// get vendor sales report records from database
  $sql_distributors = "SELECT cur_year_sales, pre_year_sales, distName, comments FROM distributors ORDER BY sort_order ASC";
  $q_distributors = $connection->prepare($sql_distributors);
  $q_distributors->execute();
  $rowCount_distributors = $q_distributors->rowCount();
  
	$lastBrokerRow     = $rowCount_sales + 5;
	$bottomHeadingRow  = $lastBrokerRow + 1;
	$beginDistActivity = $bottomHeadingRow + 1;
	$lastDistRow       = $beginDistActivity + ($rowCount_distributors - 1);
	$commentsRow       = $beginDistActivity + $rowCount_distributors;
	$rowHeightDist     = 30;
	$excelFilename     = "spreadsheets/sales_performance";

/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2013 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2013 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/New_York');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('America/New_York');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// Add some data
echo date('H:i:s') , " Add some data" , EOL;
$objPHPExcel->setActiveSheetIndex(0);

// set default style for this workbook
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(11);
$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$gThickBot       = new PHPExcel_Style();
$gThinBot        = new PHPExcel_Style();
$wThinBot        = new PHPExcel_Style();
$wThickBot       = new PHPExcel_Style();
$wThickBotBold   = new PHPExcel_Style();
$Title					 = new PHPExcel_Style();

$gThickBot = array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => '8AD38F')
							),
								'borders' => array(
								'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THICK),
								'right'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'left'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'top'			=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
							)
							);

$gThinBot = array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => '8AD38F')
							),
								'borders' => array(
								'bottom'			=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'right'				=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'left'				=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'top'					=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'vertical'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
							),
								'font'		=> array(
								'name'		=> 'Arial',
								'bold'    => TRUE							
							),
							'alignment' => array(
								'horizontal' => 'center',
								'vertical'   => 'bottom'
							)
							);
						
$wThinBot = array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => 'FFFFFF')
							),
								'borders'      => array(
								'bottom'	     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'right'		     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'left'		     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'top'			     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'vertical'	   => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'horizontal'	 => array('style' => PHPExcel_Style_Border::BORDER_THIN)

							),
								'alignment' => array(
								'horizontal' => 'center'
							)
							);
							
$wThinBotBold = array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => 'FFFFFF')
							),
								'borders'      => array(
								'bottom'	     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'right'		     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'left'		     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'top'			     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'vertical'	   => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'horizontal'	 => array('style' => PHPExcel_Style_Border::BORDER_THIN)

							),
								'font'    => array(
								'bold'    => TRUE
							)
							);
							
$wThickBot = array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => 'FFFFFF')
							),
								'borders' => array(
								'bottom'	     => array('style' => PHPExcel_Style_Border::BORDER_THICK),
								'right'		     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'left'		     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'top'			     => array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'vertical'	   => array('style' => PHPExcel_Style_Border::BORDER_THIN)
							),
								'alignment' => array(
								'horizontal' => 'center'
							),
							);
		 
$wThickBotBold = array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => 'FFFFFF')
							),
								'borders' => array(
								'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THICK),
								'right'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'left'		=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
								'top'			=> array('style' => PHPExcel_Style_Border::BORDER_THIN)
							),
								'font'    => array(
								'bold'    => TRUE
							)
							);
		 
$Title = array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => 'FFFFFF')
							),
								'font'    => array(
								'bold'    => TRUE, 								
								'name'    => 'Arial',
								'size'    => 18
							),
								'borders' => array(
								'bottom'	=> array('style' => PHPExcel_Style_Border::BORDER_THICK),
							),
								'alignment' => array(
								'horizontal' => 'center'
							)
							);

$genComHeading = array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => 'FFFFFF')
							),
								'font'    => array(
								'bold'    => TRUE, 								
								'name'    => 'Arial',
								'size'    => 14
							),
								'borders' => array(
								'outline'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							),
								'alignment' => array(
								'vertical' => 'top'
							)
							);
							
$genComments = array('fill' 	=> array(
								'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
								'color'		=> array('rgb' => 'FFFFFF')
							),
								'borders' => array(
								'outline'	=> array('style' => PHPExcel_Style_Border::BORDER_THIN),
							),
								'alignment' => array(
								'vertical'  => 'top',
								'wrap'      => TRUE
							)
							);
							
// set row heights
$genCommentsRow = $beginDistActivity + $rowCount_distributors;
for($row = 1; $row < 4; $row++) {
	$objPHPExcel->getActiveSheet()->getrowDimension($row)->setrowHeight(30);
}
for($row = 4; $row < $beginDistActivity; $row++) {
	$objPHPExcel->getActiveSheet()->getrowDimension($row)->setrowHeight(22);
}
for($row = $beginDistActivity; $row < ($genCommentsRow); $row++) {
	$objPHPExcel->getActiveSheet()->getrowDimension($row)->setrowHeight($rowHeightDist);
}
$objPHPExcel->getActiveSheet()->getrowDimension($genCommentsRow)->setrowHeight(50);

// set heading values
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Sales Performance');
$objPHPExcel->getActiveSheet()->setCellValue('B2', '2014 YTD Sales');
$objPHPExcel->getActiveSheet()->setCellValue('C2', '2013 Sales');
$objPHPExcel->getActiveSheet()->setCellValue('D2', '+/- $');
$objPHPExcel->getActiveSheet()->setCellValue('E2', '+/- %');
$objPHPExcel->getActiveSheet()->setCellValue('F2', '2014 Goal');
$objPHPExcel->getActiveSheet()->setCellValue('G2', 'Variance to Goal');
$objPHPExcel->getActiveSheet()->setCellValue('H2', '+/- $');
$objPHPExcel->getActiveSheet()->setCellValue('I2', '+/- %');
$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Broker');
$objPHPExcel->getActiveSheet()->setCellValue('A' . $bottomHeadingRow, 'Distributor Activity');
$objPHPExcel->getActiveSheet()->setCellValue('B' . $bottomHeadingRow, '2014 YTD Sales');
$objPHPExcel->getActiveSheet()->setCellValue('C' . $bottomHeadingRow, '2013 Sales');
$objPHPExcel->getActiveSheet()->setCellValue('D' . $bottomHeadingRow, '+/- %');
$objPHPExcel->getActiveSheet()->setCellValue('E' . $bottomHeadingRow, 'Comments');
$objPHPExcel->getActiveSheet()->setCellValue('A' . $commentsRow, 'General Comments:');

// set formulas for totals row
$objPHPExcel->getActiveSheet()->setCellValue('B3','=SUM(B5:B' . $lastBrokerRow . ')');
$objPHPExcel->getActiveSheet()->setCellValue('C3','=SUM(C5:C' . $lastBrokerRow . ')');
$objPHPExcel->getActiveSheet()->setCellValue('D3','=B3-C3');
$objPHPExcel->getActiveSheet()->setCellValue('E3','=ABS(B3)/C3-1');
$objPHPExcel->getActiveSheet()->setCellValue('F3','=SUM(F5:F' . $lastBrokerRow . ')');
$objPHPExcel->getActiveSheet()->setCellValue('G3','=B3-F3');
$objPHPExcel->getActiveSheet()->setCellValue('H3','=B3-F3');
$objPHPExcel->getActiveSheet()->setCellValue('I3','=G3/F3');

// insert sales data from database
$rowNumber = 5;
while($row_sales = $q_sales->fetch(PDO::FETCH_ASSOC)) { 
	$objPHPExcel->getActiveSheet()->setCellValue('A' . $rowNumber, $row_sales['brokerName']);
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $rowNumber, $row_sales['cur_year_sales']);
	$objPHPExcel->getActiveSheet()->setCellValue('C' . $rowNumber, $row_sales['pre_year_sales']);
	$objPHPExcel->getActiveSheet()->setCellValue('F' . $rowNumber, $row_sales['cur_year_goal']);
	$rowNumber++;
}

$rowNumber = $beginDistActivity;
while($row_distributors = $q_distributors->fetch(PDO::FETCH_ASSOC)) { 
	$objPHPExcel->getActiveSheet()->setCellValue('A' . $rowNumber, $row_distributors['distName'] . ' (All)');
	$objPHPExcel->getActiveSheet()->setCellValue('B' . $rowNumber, $row_distributors['cur_year_sales']);
	$objPHPExcel->getActiveSheet()->setCellValue('C' . $rowNumber, $row_distributors['pre_year_sales']);
	$objPHPExcel->getActiveSheet()->setCellValue('E' . $rowNumber, $row_distributors['comments']);
	$rowNumber++;
}

// merge cells
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:A3');
$objPHPExcel->getActiveSheet()->mergeCells('E46:I46');
$objPHPExcel->getActiveSheet()->mergeCells('E47:I47');
$objPHPExcel->getActiveSheet()->mergeCells('E48:I48');
$objPHPExcel->getActiveSheet()->mergeCells('E49:I49');
$objPHPExcel->getActiveSheet()->mergeCells('A50:I50');
$objPHPExcel->getActiveSheet()->mergeCells('E' . $bottomHeadingRow . ':I' . $bottomHeadingRow);
$objPHPExcel->getActiveSheet()->mergeCells('B' . $genCommentsRow . ':I' . $genCommentsRow);

// set cell formatting
// format broker rows
$objPHPExcel->getActiveSheet()->getStyle('B3:D3')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_RED);
$objPHPExcel->getActiveSheet()->getStyle('E3')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
$objPHPExcel->getActiveSheet()->getStyle('F3:H3')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_RED);
$objPHPExcel->getActiveSheet()->getStyle('I3')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
for($row = 5; $row < ($rowCount_sales + 5); $row++) {
	$objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':I' . $row)->applyFromArray($wThinBot);
	$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setHorizontal('left');
	$objPHPExcel->getActiveSheet()->getStyle('B' . $row . ':D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_RED);
	$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
	$objPHPExcel->getActiveSheet()->getStyle('F' . $row . ':H' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_RED);
	$objPHPExcel->getActiveSheet()->getStyle('I' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
	$objPHPExcel->getActiveSheet()->setCellValue('D'. $row,'=B' . $row . '-C' . $row);
	$objPHPExcel->getActiveSheet()->setCellValue('E'. $row,'=ABS(B' . $row . ')/C' . $row . '-1');
	$objPHPExcel->getActiveSheet()->setCellValue('G'. $row,'=B' . $row . '-F' . $row);
	$objPHPExcel->getActiveSheet()->setCellValue('H'. $row,'=B' . $row . '-F' . $row);
	$objPHPExcel->getActiveSheet()->setCellValue('I'. $row,'=G' . $row . '/F' . $row);
}

// format distributor rows
for($row = $beginDistActivity; $row <= $lastDistRow; $row++) {
	if($row == $lastDistRow) {
		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->applyFromArray($wThickBotBold);
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row . ':I' . $row)->applyFromArray($wThickBot);
	}
	else
	{
		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->applyFromArray($wThinBotBold);
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row . ':I' . $row)->applyFromArray($wThinBot);
	}
	$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setHorizontal('left');
	$objPHPExcel->getActiveSheet()->getStyle('E' . $row . ':I' . $row)->getAlignment()->setWrapText(true);
	$objPHPExcel->getActiveSheet()->getStyle('B' . $row . ':C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_RED);
	$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE);
	$objPHPExcel->getActiveSheet()->setCellValue('D'. $row,'=+B' . $row . '/C' . $row . '-1');
	$objPHPExcel->getActiveSheet()->mergeCells('E' . $row . ':I' . $row);
}

$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($Title);
$objPHPExcel->getActiveSheet()->getStyle('A2')->applyFromArray($gThinBot);
$objPHPExcel->getActiveSheet()->getStyle('B2:I2')->applyFromArray($gThinBot);
$objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($gThickBot);
$objPHPExcel->getActiveSheet()->getStyle('B3:I3')->applyFromArray($wThickBot);
$objPHPExcel->getActiveSheet()->getStyle('A4:I4')->applyFromArray($gThinBot);
$objPHPExcel->getActiveSheet()->getStyle('A' . $lastBrokerRow . ':I' . $lastBrokerRow)->applyFromArray($wThickBot);
$objPHPExcel->getActiveSheet()->getStyle('A' . $bottomHeadingRow . ':I' . $bottomHeadingRow)->applyFromArray($gThinBot);
$objPHPExcel->getActiveSheet()->getStyle('A' . $bottomHeadingRow . ':I' . $bottomHeadingRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
$objPHPExcel->getActiveSheet()->getStyle('A' . $genCommentsRow)->applyFromArray($genComHeading);
$objPHPExcel->getActiveSheet()->getStyle('B' . $genCommentsRow)->applyFromArray($genComments);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

// Save Excel 2007 file
echo date('H:i:s') , " Write to Excel2007 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save($excelFilename . '.xlsx');
/* $objWriter->save(str_replace('.php', '.xlsx', __FILE__)); */
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Save Excel 95 file
echo date('H:i:s') , " Write to Excel5 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save($excelFilename . '.xls');
/* $objWriter->save(str_replace('.php', '.xls', __FILE__)); */
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


// Echo memory peak usage
echo date('H:i:s') , " Peak memory usage: " , (memory_get_peak_usage(true) / 1024 / 1024) , " MB" , EOL;

// Echo done
echo date('H:i:s') , " Done writing file" , EOL;
echo 'File has been created in ' , getcwd() , EOL;
