<?php
require_once('../BRdbconnect.php');
require_once('../../init.php');
$adminLogin = true;

// ---------------  restrict accsss to page  ---------------------
  if (!isset($_SESSION)) {
    session_start();
  }
  // check to see if user is logged in by checking username session variable
  // redirect page if not logged in or access denied
  $accessDenied = "admin_login.php";
  if (empty($_SESSION["loginUser"])) {
    // this user is not logged in
    // redirect to login page
    header("Location: " . $accessDenied);
  }
  else
  {
	  if(!isset($_SESSION['loginRole']) || $_SESSION['loginRole'] != "1") {
		  // this user is not an admin user
		  // redirect to login page
			header("Location: " . $accessDenied);
	  }
  }
// ---------------  end restrict accsss  ---------------------
  
  $loginId      = $_SESSION["loginId"];

  // get report records from database
  $sql_reports = "SELECT broker_reports.*, brokerName FROM broker_reports, user WHERE user.Id = broker_reports.userId ORDER BY brokerName ASC";
  $q_reports = $connection->prepare($sql_reports);
  $q_reports->execute();
  $rowCount_reports = $q_reports->rowCount();
  
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	
	<!-- TODO: Set page title -->
	<title><?=Config::$siteName?></title>

	<!-- TODO: Set page description. -->
	<meta name="description" content="<?=Config::$metaDescription?>">

	<!-- TODO: Change Favicon. -->
	<link rel="icon" type="image/png" href="<?=Config::$siteRoot?>/favicon.png">

	<!-- Styles and Scripts -->
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/main.css">
  <link href="<?=Config::$siteRoot?>/broker_reporting/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/broker_reporting/css/brokerReporting.css">
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/broker_reporting/css/jquery.dataTables.css">
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
	<script src="../js/jquery.dataTables.min.js" ></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('#reports').dataTable( {
/* 				"sPagination": true, */
/* 				"bJQueryUI": true, */
				"bAutoWidth": false,
				"aoColumns": [{ "sWidth": "21%" }, { "sWidth": "21%" }, { "sWidth": "10%" }, { "sWidth": "12%" }, { "sWidth": "12%" }, { "sWidth": "24%" }],
				/* "sDom": '<"H"fl><t><"ui-helper-clearfix"ip>', */
				"sDom": '<"H"<"selectQuarter"l>f><t><"ui-helper-clearfix"ip>',
				"sPaginationType":"full_numbers"
			});
		});	
	</script>

</head>
<body>
	<?php include('../header.php'); ?>
			<div class="content">
				<div class="row">  <!-- row 1 -->
					<div class="col-md-10 col-md-offset-1">  <!-- column 1-1 -->
						<div class="adminHeading"> ADMIN HOME</div>
						<div class="adminButtons">
							<input type="button" onclick="location.href='#'" value="DOWNLOAD INDIVIDUAL BROKER (EXCEL)" class="button">
							<input type="button" onclick="location.href='#'" value="DOWNLOAD MASTER (EXCEL)" class="button" style="margin-left:10px">
							<input type="button" onclick="location.href='#'" value="UPLOAD MASTER (EXCEL)" class="button" style="margin-left:10px">
						</div>
					</div> <!-- end column 1-1 -->
				</div> <!-- end row 1 -->
					
				<div class="row">  <!-- row 2 -->
					<div class="col-md-10 col-md-offset-1">  <!-- column 1-2 -->
						<div style="margin-top:30px">
							<div class="formInstruction">Viewing Current Quarter - Change to 
								<select name="selectQuarter" class="customSelect">
										<option value="2013 - Q3">2013 - Q3</option>
										<option value="2013 - Q2">2013 - Q2</option>
										<option value="2013 - Q1">2013 - Q1</option>
										<option value="2012 - Q4">2012 - Q4</option>
										<option value="2012 - Q3">2012 - Q3</option>
								</select>
								<input type="submit" class="button" value="GO">
							</div>
						</div>
					</div>  <!-- end column 1-2 -->
				</div>  <!-- end row 2 -->
				
				<div class="row">  <!-- row 3 -->
					<div class="col-md-10 col-md-offset-1">  <!-- column 1-3 -->
						<div class="title1">Broker Reports Quick View</div>
						<table id="reports">
					    <thead>
					        <tr>
					            <th>Broker</th>
					            <th>Restaurant Name</th>
					            <th>Total Units</th>
					            <th>Results</th>
					            <th>Tools Utilized</th>
					            <th>Comments</th>
					        </tr>
					    </thead>
						  <tbody>
					    		<?php while($row = $q_reports->fetch(PDO::FETCH_ASSOC)) { ?>
						    	<tr>
					            <td><?php echo $row['brokerName']; ?></td>
					            <td><?php echo $row['restName']; ?></td>
					            <td><?php echo $row['units']; ?></td>
					            <td><?php echo $row['results']; ?></td>
					            <td><?php echo $row['tools']; ?></td>
					            <td id="comments"><?php echo $row['comments']; ?></td>
					        </tr>
					    		<?php } ?>
					    </tbody>
					</table>
					<div style="margin-bottom:30px"></div>
					</div>  <!-- end column 1-3 -->
				</div>  <!-- end row 3 -->
			</div>  <!-- end content -->
				
  <script src="../js/bootstrap.js"></script>

</body>
</html>