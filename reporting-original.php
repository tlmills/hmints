<?php
require_once('BRdbconnect.php');
require_once('../init.php');

  // Restrict accsss to page
  if (!isset($_SESSION)) {
    session_start();
  }
  // check to see if user is logged in by checking username session variable
  // redirect page if not logged in or access denied
  $accessDenied = "index.php";
  if (empty($_SESSION["loginUser"])) {
    // this user is not logged in
    // redirect to login page
    header("Location: " . $accessDenied);
  }

  // get user record from database
  $userId = $_SESSION["loginId"];

  $sql = "SELECT id, brokerName FROM user WHERE id = :id";
  $q_user = $connection->prepare($sql);
  $q_user->execute(array('id' => $userId));
  $r_user = $q_user->fetch(PDO::FETCH_ASSOC);

  // get records for all books
  // query
/*
  $sql_books = "SELECT bookId, title, authorFirst, authorLast, quantity, publisher, genre FROM books, genres WHERE genres.genreId = books.genreId ORDER BY title ASC";
  $q_books = $conn->prepare($sql_books);
  $q_books->execute();
*/
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<!-- TODO: Set page title -->
	<title><?=Config::$siteName?></title>

	<!-- TODO: Set page description. -->
	<meta name="description" content="<?=Config::$metaDescription?>">

	<!-- TODO: Change Favicon. -->
	<link rel="icon" type="image/png" href="<?=Config::$siteRoot?>/favicon.png">

	<!-- Styles -->
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/main.css">
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/broker_reporting/css/brokerReporting.css">

</head>
<body>
	<?php include('header.php'); ?>
	<div class="wrapper">	
		<div class="content">
			<div class="leftSide">
				<form action="reporting.php" id="reportForm" method="post" accept-charset="utf-8">
					<div class="errorMsg"><?php echo $errorMsg; ?></div>
					<div> <h1>Welcome <?php echo $r_user['brokerName']; ?></h1> </div>
					<div style="color:#999999; font-size:16px"> Please complete the following information: </div>
			    <div class="formPrompt"><label>Restaurant Name:</label></div>
			    <div class="formField"><input type="text" name="restName" value="" size="40" maxlength="255" /></div>	
			    <div class="formPrompt"><label>Number of Units:</label></div>
			    <div class="formField"><input type="text" name="units" value="" size="20" maxlength="50" /></div>
			    <div class="formPrompt"><label>Results:</label></div>
			    <div class="formField">
			    	<select>
							<option value="" selected="selected">Please Choose...</option>
							<option value="Closed">Closed</option>
							<option value="In Progress">In Progress</option>
							<option value="Denied">Denied</option>
							<option value="Bounty Paid">Bounty Paid</option>
						</select>
			    </div>	    
			    <div class="formPrompt"><label>Tools Utilized:</label></div>
			    <div class="formField">
			    	<select>
							<option value="" selected="selected">Please Choose...</option>
							<option value="Story Board">Story Board</option>
							<option value="Art Proof">Art Proof</option>
							<option value="Samples">Samples</option>
							<option value="None">None</option>
						 </select>
			    </div>
			    <div class="formPrompt"><label>Comments:</label></div>
			    <div class="formField"><textarea name="comments" value="" rows="7" cols="40"></textarea></div>
			    <div class="formPrompt"></div>
			    <div class="buttonRow"><img src="../brokers/images/0.png" class="buttonLeft"><input class="button" type="submit" name="signin" value="SUBMIT REPORT" /><img src="../brokers/images/0.png" class="buttonRight">
			    </div>
				</form>
			</div>
		</div>	
	</div>
</body>