<?php require_once('BRdbconnect.php');
  
  // start session if not already started
  if (!isset($_SESSION)) {
    session_start();
  }

  // Restrict accsss to page

  // check to see if user is logged in by checking username session variable
  // redirect page if not logged in or access denied
  $accessDenied = "index.php";
  if (empty($_SESSION["loginUser"])) {
    // this user is not logged in; redirect to login page
    header("Location: " . $accessDenied);
  }
  
  // get record from users table
  $restName = $_POST["restName"];
  if(isset($_POST["originalName"])) {
		$originalName = $_POST["originalName"];
  }

  $sql_name = "SELECT restName FROM broker_reports WHERE restName = :restName";
  $q_name = $connection->prepare($sql_name);
  $q_name->execute(array('restName' => $restName));
  $r_name = $q_name->fetch(PDO::FETCH_ASSOC); 
  $rowCount = $q_name->rowCount();
  
  if($rowCount > 0) { 
      // restaurant name was found in database      
      // if this is an edit of an existing record, check to see if restaurant name is associated with the current record being edited in report_edit.php
      if(isset($originalName) && ($originalName == $r_name['restName'])) {
	    	$result = false;
      }
      else
      {
	  		$result = true;
      }
  } 
  else
  {
      // restaurant name was NOT found in database; restaurant name can be used with this report
      $result = false;
  }
  
echo json_encode($result);
?>