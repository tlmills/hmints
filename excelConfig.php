<?php /* This file is included on every page in the site. Treat it delicately. */

//Set the timezone so any date operations work as expected.
date_default_timezone_set("America/New_York");

class Config{
	// Configuration for downloading sales info from database to Excel spreadsheet
	
	//Database Info
	const dbUser = 'hmints_hmints';
	const dbPass = 'g);x2cHpxtU.';
	const dbHost = 'localhost';
	const dbSchema = 'hmints_hmints';
	
	//Enable for Debugging
	const DEBUG = FALSE;	
}

class MyTools{
	public static function slugToTitle($slug){
		return ucwords(str_ireplace(array('-', '_'), ' ', $slug));
	}
	
	public static function titleToSlug($title){
		return strtolower(str_ireplace(' ', '-', $title));
	}
}

//Connect to the Database
class StellarMySQLi extends mysqli{
	function query_and_fetch_assoc($query){
		$results = $this->query($query);
		if($this->errno > 0){
			return FALSE;
		}
		if($results->num_rows > 0){			
			for($i = 0; $i < $results->num_rows; $i++){
				$rows[] = $results->fetch_assoc();
			}
			return $rows;
		}else{
			return FALSE;
		}
	}
	function insert_array($table, $data, $exclude = array()) {
	
	    $fields = $values = array();
	
	    if( !is_array($exclude) ) $exclude = array($exclude);
	
	    foreach( array_keys($data) as $key ) {
	        if( !in_array($key, $exclude) ) {
	            $fields[] = "`$key`";
	            $values[] = "'" . mysqli_real_escape_string($this, $data[$key]) . "'";
	        }
	    }
	
	    $fields = implode(",", $fields);
	    $values = implode(",", $values);
	
	    if( mysqli_query($this, "INSERT INTO `$table` ($fields) VALUES ($values)") ) {
	        return array( "mysql_error" => false,
	                      "mysql_insert_id" => mysqli_insert_id($this),
	                      "mysql_affected_rows" => mysqli_affected_rows($this),
	                      "mysql_info" => mysqli_info($this)
	                    );
	    } else {
	        return array( "mysql_error" => mysqli_error($this) );
	    }
	
	}
}//end class StellarMySQLi

$db = new StellarMySQLi(Config::dbHost, Config::dbUser, Config::dbPass, Config::dbSchema);
$db->set_charset('utf8');
$db->query("SET time_zone = 'America/New_York'");

$ink_colors = array(
    'white' => '#FFFFFF',
    'red' => '#bf2c37',
    'burgundy' => '#922c46',
    'purple' => '#6c1f7e',
    'navy_blue' => '#003d7d',
    'royal_blue' => '#005dab',
    'green' => '#007550',
    'orange' => '#f68938',
    'brown' => '#571a04',
    'black' => '#000000',
    'metallic_gold' => '#a89a6f',
    'metallic_silver' => '#a7a9ac',
    'pink' => '#f9c5c6',
    'lime' => '#8cc63f'
);

$state_list = array(
	'AL'=>"Alabama",  
	'AK'=>"Alaska",  
	'AZ'=>"Arizona",  
	'AR'=>"Arkansas",  
	'CA'=>"California",  
	'CO'=>"Colorado",  
	'CT'=>"Connecticut",  
	'DE'=>"Delaware",  
	'DC'=>"District Of Columbia",  
	'FL'=>"Florida",  
	'GA'=>"Georgia",  
	'HI'=>"Hawaii",  
	'ID'=>"Idaho",  
	'IL'=>"Illinois",  
	'IN'=>"Indiana",  
	'IA'=>"Iowa",  
	'KS'=>"Kansas",  
	'KY'=>"Kentucky",  
	'LA'=>"Louisiana",  
	'ME'=>"Maine",  
	'MD'=>"Maryland",  
	'MA'=>"Massachusetts",  
	'MI'=>"Michigan",  
	'MN'=>"Minnesota",  
	'MS'=>"Mississippi",  
	'MO'=>"Missouri",  
	'MT'=>"Montana",
	'NE'=>"Nebraska",
	'NV'=>"Nevada",
	'NH'=>"New Hampshire",
	'NJ'=>"New Jersey",
	'NM'=>"New Mexico",
	'NY'=>"New York",
	'NC'=>"North Carolina",
	'ND'=>"North Dakota",
	'OH'=>"Ohio",  
	'OK'=>"Oklahoma",  
	'OR'=>"Oregon",  
	'PA'=>"Pennsylvania",  
	'RI'=>"Rhode Island",  
	'SC'=>"South Carolina",  
	'SD'=>"South Dakota",
	'TN'=>"Tennessee",  
	'TX'=>"Texas",  
	'UT'=>"Utah",  
	'VT'=>"Vermont",  
	'VA'=>"Virginia",  
	'WA'=>"Washington",  
	'WV'=>"West Virginia",  
	'WI'=>"Wisconsin",  
	'WY'=>"Wyoming"
);