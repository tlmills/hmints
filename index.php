<?php
require_once('BRdbconnect.php');
require_once('../init.php');

	// set error message
	if (isset($_GET["errMsg"])) {
		$errMsg = $_GET["errMsg"];		
		switch($errMsg) {
			case "notBroker":
				$errorMsg = "This login is for brokers only.";
				break;
			case "notValid":
				$errorMsg = "Invalid email/password combination. </br> Please try your login again.";
				break;
			default:
				$errorMsg = "";
		}
	}

  // Check login credentials
  if (!isset($_SESSION)) {
    session_start();
  }

  if (isset($_POST["email"])) {
    $email           = $_POST["email"];
    $pass            = $_POST["pass"];
    $redirectFail    = "index.php";
		$redirectSuccess = "reporting.php";
		$errorMsg        = "";

		// attempt to select user record from database
    $sql = "SELECT id, roleId FROM user WHERE email = :email AND pass = :pass";
		$q = $connection->prepare($sql);
    $q->execute(array('email' => $email,
    									'pass'  => $pass));

    $rowCount = $q->rowCount();
		
    if ($rowCount == 1) {
      // a record was found with the email and password entered by the user
      // get the fields from the returned record
      $result = $q->fetch(PDO::FETCH_ASSOC);

      if ($result['roleId'] == 2) {
	    	// set session variables
	      $_SESSION["loginUser"]     = $email;
	      $_SESSION["loginPassword"] = $pass;
	      $_SESSION["loginId"]       = $result['id'];
	      $_SESSION["loginRole"]     = $result['roleId'];
	      
	      // redirect to login success page 
				$redirectSuccess = $redirectSuccess . "?id=" . $result['id'];
				header("Location: " . $redirectSuccess);
			}
			else
			{
				// not a broker - redirect to login page
				$redirectFail = $redirectFail . "?errMsg=notBroker";
				header("Location: " . $redirectFail);
			}
		}
    else 
    {
      // no record found - redirect to login fail page
			$redirectFail = $redirectFail . "?errMsg=notValid";
      header("Location: ". $redirectFail);
    }
  }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- TODO: Set page title -->
	<title><?=Config::$siteName?></title>

	<!-- TODO: Set page description. -->
	<meta name="description" content="<?=Config::$metaDescription?>">

	<!-- TODO: Change Favicon. -->
	<link rel="icon" type="image/png" href="<?=Config::$siteRoot?>/favicon.png">

	<!-- Styles -->
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/main.css">
	<!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/broker_reporting/css/brokerReporting.css">

</head>
<body>
	<?php include('header.php'); ?>
	<div class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-3">
					<form action="index.php" method="post">
						<div class="formPrompt"></div>
						<div class="formField">
							<div class="errorMsg"><?php echo $errorMsg; ?></div>
						</div>
						<div class="formPrompt"></div>
						<div class="formField" style="color:#999999; font-size:16px"> Welcome, please sign-in </div>
				    <div class="formPrompt"><label>Email:</label></div>
				    <div class="formField">
				    	<input type="text" name="email" value="" size="40" maxlength="255" />
				    </div>
				    <div class="formPrompt"><label>Password:</label></div>
				    <div class="formField">
				    	<input type="password" name="pass" value="" size="40" maxlength="50" />
				    </div>
				    <div class="formPrompt"></div>
				    <div class="formField">
					    <div class="buttonRow"><input class="button" style="width:150px" type="submit" name="signin" value="SIGN IN" /></div>
				    </div>
				    <div class="formPrompt"></div>
				    <div class="formField">
				    	<div><a href="<?php echo $_SERVER['PHP_SELF']. '?forgotPass=1'; ?>">Forgot your password?</a></div>
				    </div>
				    <div class="formPrompt"></div>
				    <div class="formField">
				    	<div style="margin-left:270px; padding-top:30px"><a href="#">Privacy Policy</a></div>
				    </div>
					</form>
			</div>  <!-- end column -->
		</div>  <!-- end row -->
	</div>	<!-- end content -->
	
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="js/bootstrap.js"></script>
</body>
</html>