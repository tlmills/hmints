<?php
require_once('BRdbconnect.php');
require_once('../init.php');

  // Restrict accsss to page
  if (!isset($_SESSION)) {
    session_start();
  }
  // check to see if user is logged in by checking username session variable
  // redirect page if not logged in or access denied
  $accessDenied = "index.php";
  if (empty($_SESSION["loginUser"])) {
    // this user is not logged in
    // redirect to login page
    header("Location: " . $accessDenied);
  }
  
  $loginId      = $_SESSION["loginId"];
  $confirmOnOff = "none";
  $formOnOff    = "inline";
  
  // update report record in database
  // check value of hidden form field to see if form has been submitted
  if ((isset($_POST["formSubmitted"])) && ($_POST["formSubmitted"]) == "y") {

    $restName     = trim($_POST["restName"]);
    $originalName = $_POST['originalName'];
    $units   		  = trim($_POST["units"]);
    $results      = $_POST["results"];
    $tools        = $_POST["tools"];
    $comments     = $_POST["comments"];
		$reportId     = $_POST["reportId"];
    
    // validate form data
    $validateError = "";
    
    // restaurant name
    if(empty($restName) || strlen(trim($restName)) == 0) {
	    $validateError .= "<li>Please enter the restaurant name.</li>";
    }
    else
    {
		  //  check if a report for this restaurant is already in the database
		  if($restName != $originalName) {
				$sql_check = "SELECT id, restName FROM broker_reports WHERE restName = :restName";
			  $q_check = $connection->prepare($sql_check);
			  $q_check->execute(array('restName' => $restName));
			  $r_check = $q_check->fetch(PDO::FETCH_ASSOC);
			  $rowCount_check = $q_check->rowCount();
			  
			  if($rowCount_check > 0) {
				  $validateError .= "<li>A report for " . $restName . " already exists.</li>";
			  }
		  }
		}
    
    // number of units
    if(empty($units) || !is_numeric($units)) {
	    $validateError .= "<li>Please enter the number of units.</li>";
    }
    
    // results
    if(empty($results)) {
		    $validateError .= "<li>Please choose the result.</li>";
    }
    
    // tools utilized
    if(empty($tools)) {
		    $validateError .= "<li>Please choose the tools utilized.</li>";
    }
    
    if(empty($validateError)) {
	
	    $sql = "UPDATE broker_reports SET restName = :restName, units = :units, results = :results, tools = :tools, comments = :comments WHERE id = :reportId";
	    $q_query = $connection->prepare($sql);
	    $q_query->execute(array('restName'    =>$restName,
	                      	    'units'       =>$units,
	                      	    'results'     =>$results,
	                      	    'tools'       =>$tools,
	                      	    'comments'    =>$comments,
	                      	    'reportId'    =>$reportId));
	   
	    $confirmOnOff = "inline";
	    $formOnOff    = "none";
    }
  }
  else
  {
    $reportId    = $_GET["reportId"];
	  
	  // get report record from database
	  $sql_edit = "SELECT id, restName, units, results, tools, comments, submitDate FROM broker_reports WHERE id = :reportId";
	  $q_edit = $connection->prepare($sql_edit);
	  $q_edit->execute(array('reportId' => $reportId));
	  $r_edit = $q_edit->fetch(PDO::FETCH_ASSOC);

	  $restName    = $r_edit['restName'];
    $units   		 = $r_edit['units'];
    $results     = $r_edit['results'];
    $tools       = $r_edit['tools'];
    $comments    = $r_edit['comments'];
    $submitDate  = date('m/d/Y', strtotime($r_edit['submitDate']));
  }

  // get user record from database
  $sql = "SELECT id, brokerName FROM user WHERE id = :id";
  $q_user = $connection->prepare($sql);
  $q_user->execute(array('id' => $loginId));
  $r_user = $q_user->fetch(PDO::FETCH_ASSOC);
  
  // get reports user has submitted
  $sql_reports = "SELECT id, restName, submitDate FROM broker_reports WHERE userId = :loginId ORDER BY submitDate DESC";
  $q_reports = $connection->prepare($sql_reports);
  $q_reports->execute(array('loginId' => $loginId));
  $rowCount_reports = $q_reports->rowCount();
  
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">	
	
	<!-- TODO: Set page title -->
	<title><?=Config::$siteName?></title>

	<!-- TODO: Set page description. -->
	<meta name="description" content="<?=Config::$metaDescription?>">

	<!-- TODO: Change Favicon. -->
	<link rel="icon" type="image/png" href="<?=Config::$siteRoot?>/favicon.png">

	<!-- Styles and Scripts -->
	<link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/main.css">
  <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="css/brokerReporting.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>
	<script src="js/jquery.validate.min.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function()  { 
		 
		// form validation
		  $("#reportForm").validate({
		  	errorContainer: "#errorContainer",
		    errorLabelContainer: "#errorContainer",
		    errorElement: "li",
		    rules: {
		      restName: {
		      	required: true
		      },
		      units: {
		        required: true,
						digits: true
		      },
		      results: {
		        required: true
		      },
		      tools: {
		        required: true
		      }
		    },
		    messages: {
		      restName: "Please enter the restaurant name.",
		      units:  {
					  required: "Please enter the number of units.",
					  digits: "Please enter numbers only."
				  },
				  results: "Please choose a result.",
				  tools: "Please choose the tools utilized."
		    }
		  });  // end of $("#reportForm").validate
		});  // end of $(document).ready
	</script>

</head>
<body>
	<?php include('header.php'); ?>
			<div class="content">
				<div class="row">  <!-- row 1 -->
					<div class="col-md-6">  <!-- column 1-1 -->
					<div style="margin-left:26px"> <h1>Welcome <?php echo $r_user['brokerName']; ?></h1></div>

							<div style="display:<?php echo $confirmOnOff; ?>">
								<div class="formPrompt" style="margin-left:30px; margin-bottom:20px"> We have received your changes to this report, thank you. </div>
								<div style="clear:left; margin-left:30px"><a href="reporting.php" class="button" style="padding:5px 12px; text-decoration:none; color:white">submit a new report</a></div>
							</div>
							
							<ol id="errorContainer" style="display: none"></ol>
							<?php if(!empty($validateError)) {
								echo "<div id='validateMessage'> <ol> $validateError </ol> </div>";
								} 
							?>
							<div style="display:<?php echo $formOnOff; ?>">							
								<div style="color:#999999; font-size:16px; margin-left:30px">Please make any necessary changes to the following report:  </div>
								<div style="color:#999999; font-size:16px; margin-bottom:20px; margin-left:30px">This report was originally submitted on <?php echo $submitDate; ?>  </br></div>
						    <form action="report_edit.php" id="reportForm" method="post">
						    <div class="formPrompt"><label>Restaurant Name:</label></div>
						    <div class="formField"><input type="text" name="restName" id="restName" value="<?php echo $restName; ?>" size="40" maxlength="255" /></div>	
						    <div class="formPrompt"><label>Number of Units:</label></div>
						    <div class="formField"><input type="text" name="units" id="units" value="<?php echo $units; ?>" size="20" maxlength="50" /></div>
						    <div class="formPrompt"><label>Results:</label></div>
						    <div class="formField">
						    	<select name="results" id="results">
										<option value="<?php echo $results; ?>" selected="selected">
											<?php if(empty($results)) {
												echo "Please Choose...";
											} 
											else
											{
												echo $results;
											}
											?>
										</option>
										<option value="Closed">Closed</option>
										<option value="In Progress">In Progress</option>
										<option value="Denied">Denied</option>
										<option value="Bounty Paid">Bounty Paid</option>
									</select>
						    </div>	    
						    <div class="formPrompt"><label>Tools Utilized:</label></div>
						    <div class="formField">
						    	<select name="tools" id="tools">
										<option value="<?php echo $tools; ?>" selected="selected">
											<?php if(empty($tools)) {
												echo "Please Choose...";
											} 
											else
											{
												echo $tools;
											}
											?>
										</option>
										<option value="Story Board">Story Board</option>
										<option value="Art Proof">Art Proof</option>
										<option value="Samples">Samples</option>
										<option value="None">None</option>
									 </select>
						    </div>
					    	<div class="formPrompt"><label>Comments:</label></div>
					    	<div style="float:left">
										<div class="formField"><textarea name="comments" id="comments" value=""><?php echo $comments; ?></textarea></div>
								    <div class="buttonRow">
								    	<input type="submit" class="button" value="update report"/>
									</div>					    
							 </div>
							 <input type="hidden" name="formSubmitted" value="y">
							 <input type="hidden" name="reportId" value="<?php echo $reportId; ?>">
							 <input type="hidden" name="originalName" value="<?php echo $restName; ?>">
							</form>
						</div>					
					</div> <!-- end column 1-1 -->
					<div class="col-md-6">  <!-- column 2-1 -->
					<div class="darkGreenBox">
						<?php 
							if($rowCount_reports > 0) {
								if($rowCount_reports > 1) {
									$reportWord = "reports";
								}
								else
								{
									$reportWord = "report";
								}
								echo "You have " . $rowCount_reports . " submitted " . $reportWord . " for this quarter. </br>Click on a report to make changes. </br></br>";
								
								echo "<div class='innerBox'>";
								while($row = $q_reports->fetch(PDO::FETCH_ASSOC)) {
									echo "<a href='report_edit.php?reportId=" . $row['id'] . "'>" . date('m/d/Y', strtotime($row['submitDate'])) . " - " . $row['restName'] . "</a></br>";
								}
								echo "</div>";
							} 
							else
							{
								echo "You have 0 submitted reports for this quarter";
							}	
						?>
					</div>			
					</div> <!-- end column 2-1 -->			
					</div> <!-- end row 1 -->
				</div>  <!-- end content -->
				
  <script src="js/bootstrap.js"></script>

</body>
</html>